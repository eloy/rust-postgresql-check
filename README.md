# Build
```
cargo build --release
```

# Install (example)
```
sudo cp target/release/postgresql-check /etc/sensu/plugins/
```

# FAQ

## Why another postgresql nagios/sensu check?

 Because. I want to learn Rust and I always learn new language by writting short but useful programms.

## Why Rust?

 Because it's new, it's different and it's fun.

## Go is better.

 Sure.

## But this check has 1.6MB itself!

 Stripped has 1.1MB.

## But my python check has 20 lines of code and works better!

 Use your python check.
