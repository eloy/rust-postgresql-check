extern crate postgres;
extern crate clap;
extern crate openssl;

use clap::{App, Arg};
// use openssl::ssl::{SslContext, SslMethod};

use postgres::{Connection, SslMode, ConnectTarget, ConnectParams, UserInfo};
use std::process::exit;

fn main() {


	// defaults
	let _user      = "postgres";
	let _host      = "127.0.0.1";
	let _database  = "template1";
	let _port 	   = "5432";
	let _ssl       = false;
	let _password  = "";
	let _debug     = false;

	// 1. parse json file (if available)


	// 2. parse arguments
	let _msg_host = format!("Host for postgresql connection. Default value is {}.", _host);
	let _msg_user = format!("User for postgresql connection. Default value is {}.", _user);
	let _msg_port = format!("Port for postgresql connection. Default value is {}.", _port);

	let matches = App::new("postgresql-check")
		.author("Krzysztof Krzyżaniak (eloy) <eloy@kofeina.net>")
		.version("0.0.1")
		.about("Simple postgresql check compatible with sensu (also in future! does some metrics)")
		.arg(
			Arg::with_name("ssl")
			.long("ssl")
			.takes_value(false)
			.help("Use ssl in connection.")
		)
		.arg(
			Arg::with_name("check")
			.short("c")
			.long("check")
			.takes_value(false)
			.help("Run as check")
		)
		.arg(
			Arg::with_name("port")
			.short("p")
			.long("port")
			.takes_value(true)
			.help( & * _msg_port )
		)
		.arg(
			Arg::with_name("user")
			.short("u")
			.long("user")
			.takes_value(true)
			.help( & * _msg_user )
		)
		.arg(
			Arg::with_name("password")
			.short("P")
			.long("password")
			.takes_value(true)
			.help("Password for postgresql connection. Default is no password.")
		)
		.arg(
			Arg::with_name("host")
			.short("h")
			.long("host")
			.takes_value(true)
			.help( & * _msg_host )
		)
		.arg(
			Arg::with_name("database")
			.short("d")
			.long("database")
			.takes_value(true)
			.help("Database for postgresql connection. Default is template1")
		)
		.get_matches();

	let use_ssl  = matches.is_present("ssl");
	let user     = matches.value_of("user").unwrap_or(_user);
	let host     = matches.value_of("host").unwrap_or(_host);
	let database = matches.value_of("database").unwrap_or(_database).to_string();
	let password = matches.value_of("password").unwrap_or(_password);
	let port     = matches.value_of("port").unwrap_or(_port);

	if _debug {
		println!("user {:?}", user);
		println!("host {:?}", host);
		println!("database {:?}", database);
		println!("ssl {:?}", use_ssl);
		println!("password {:?}", password);
		println!("port {:?}", port);
	}

	// 3. connect to database and return true if check
	let params = ConnectParams {
		target: ConnectTarget::Tcp(host.to_string()),
		port: Some(port.parse::<u16>().unwrap()),
		user: Some(UserInfo{
			user: user.to_string(),
			password: Some(password.to_string())
		}),
		database: Some(database.to_string()),
		options: vec![],
	};

	let conn = Connection::connect(params, &SslMode::None);
	match conn {
		Ok(ok)  => {
			println!("OK {:?}", ok);
			exit(0)
		},
		Err(err) => {
			println!("CRITICAL {}", err.to_string());
			exit(2)
		},
	}
	// 4. get statistics and return  them if  metrics
}
